**A simple Unix-like C PRNG initializer**

This project is a small tool dependency for other projects when it's about to initialize the C library PRNG on Unix-like systems (which must possess a /dev/urandom file).

After a git-clone a simple `make' in the project directory will build static and shared libraries, the C header prng.h is located in the src directory.

There is currently only one function (init_PRNG) to use as shown in the example below:

	#include "prng.h"

	int main()
	{
		// Init the C PRNG once and for all
		init_PRNG();

		// Generate a random number
		long rand_number = random();
	}

That's all!

**Contact**

freesec2021 - at - protonmail.com 
