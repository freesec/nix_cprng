/**
 *   nix_cprng -- A simple Unix-like C PRNG initializer
 *   Author: freesec2021 at protonmail.com 
 *   Copyright (C) 2019 freesec (pseudonym)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "prng.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int PRNG_INIT = 0;

void init_PRNG()
{
	if (!PRNG_INIT)
	{
		FILE *f = fopen("/dev/urandom", "r");
		uint rand;
		fscanf(f, "%u", &rand);
		// init the seed with current time
		// and linux noise (from non-blocking /dev/urandom)
		srandom(rand ^ time(NULL));
		// NOTE: the /dev/random version is blocking but
		// ensures to get fresh noise when the system has just booted
		// (read `man urandom' for randomness considerations)
		PRNG_INIT = 1;
		fclose(f);
	}
}
